import 'globals/dws_globals';
import './index.scss';


// Page related components
import '../../../components/StageModules/m310_Editorial_Stage/index';
import '../../../components/ContentModules/mk154_Feature_Wrapper/index';
import '../../../components/ContentModules/m101_Intro_Copy/index';
import '../../../components/ContentModules/m102_Copy/index';
import '../../../components/ContentModules/m104_Table/index';
import '../../../components/ContentModules/m151_Introcopy/index';


// Optional components


if(document.querySelectorAll('.m532').length) {
  require.ensure([], (require) => {
    require('../../../components/NavigationModules/m532_Showroom_Navigation/index');
  });
}
if(document.querySelectorAll('.mk268').length) {
  require.ensure([], (require) => {
    require('../../../components/TeaserModules/mk268_Trims_List/index');
  });
}

if(document.querySelectorAll('.m105').length) {
  require.ensure([], (require) => {
    require('../../../components/ContentModules/m105_Media_Gallery');
  });
}

// HMR setup
if (module.hot) {
  module.hot.accept();
}
