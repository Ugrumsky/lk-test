import 'globals/globals';
import './index.scss';


// Mandatory components
/*
import '../../../components/StageModules/m301_Homepage_Stage/index';
import '../../../components/TeaserModules/m200_Basic_Content_Teaser/index';
import '../../../components/TeaserModules/m245_Modelaccess_Teaser/index';
import '../../../components/TeaserModules/mk267_Stock_List/index';
import '../../../components/FunctionalModules/mk611_CTA_Form_Full_Width/index';
import '../../../components/ContentModules/m155_Highlight_Module/index';
import '../../../components/ContentModules/m103_Copy_Media/index';

import '../../../components/ContentModules/m108_Accordion/index';
*/

// HMR setup
if (module.hot) {
  module.hot.accept();
}
