/**
 * Created by ufs on 3/17/2017.
 */
import $ from 'jquery';
import Component, {findComponent} from '../../../globals/js/Libraries/component';
import Slider from '../../../globals/js/Libraries/slider';
import _debounce from 'lodash/debounce';
// Import styles
import './index.scss';

class TrimSelector extends Component {
  constructor(el) {
    super(el);
    const self = this;

    this.inner = $(el).find('.m246__inner');
    this.dots = $(el).find('.m246__dots');

    this.slider = new Slider(this.inner, {
      appendDots: this.dots,
    });

    this.initComponent();

    $(() => {
      self.initSlider();
      $(window).resize(_debounce(() => self.initSlider(), 200));
    });
  }

  initSlider() {
    if($(window).width() < 768) {
      this.slider.mount();
    } else {
      this.slider.unmount();
    }
  }
}


findComponent('.m246', TrimSelector);
