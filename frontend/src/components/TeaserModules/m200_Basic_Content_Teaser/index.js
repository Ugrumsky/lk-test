// Import styles
import './index.scss';

import $ from 'jquery';


export default class BasicContentTeaser {
  constructor(element) {
    this.element = element;

    this.image = element.find('.m200__img-16x9');
    this.icon = element.find('.m200__img-1x1');
    this.subline = element.find('.m200__subline');
    this.headline = element.find('.m200__headline');
    this.content = element.find('.m200__content');
  }
}

export class BasicContentTeaserCollection {
  constructor(elements) {
    this.collection = $.map(elements, (el) => new BasicContentTeaser($(el)));
  }

  applyToAll(method, ...args) {
    for(let i = 0; i < this.collection.length; i++) {
      this.collection[i][method](...args);
    }
  }
}


// $('#test_m200').find('.m200').each((index, el) => new m200BasicContentTeaser($(el)));
