import arrow from './arrow/index.pug';
import './index.scss';


function getArrow(direction, theme) {
  theme = theme || false;
  return arrow({
    direction: direction,
    theme: theme ? 'vwd5_arrow--' + theme: '',
  });
}


export default {
  left: getArrow('left'),
  right: getArrow('right'),
};

/**
 *
 * @param container Container el to which slick will append arrows
 * @returns {{arrows: boolean, prevArrow, nextArrow, appendArrows: *}}
 */
export function slickArrowsConf(container, theme) {
  return {
    arrows: true,
    prevArrow: getArrow('left', theme),
    nextArrow: getArrow('right', theme),
    appendArrows: container,
  };
}

export function arrowTheme(theme) {
  return {
    left: getArrow('left', theme),
    right: getArrow('right', theme),
  };
}
