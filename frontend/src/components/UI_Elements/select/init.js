import Choices from 'choices.js';

export function defaultSelectElementInit(el) {
  new Choices(el, {
    classNames: {
      containerOuter: 'select',
      containerInner: 'select__inner',
      input: 'select__input',
      inputCloned: 'select__input--cloned',
      list: 'select__list',
      listItems: 'select__list--multiple',
      listSingle: 'select__list--single',
      listDropdown: 'select__list--dropdown',
      item: 'select__item',
      itemSelectable: 'select__item--selectable',
      itemDisabled: 'select__item--disabled',
      itemChoice: 'select__item--choice',
      group: 'select__group',
      groupHeading: 'select__heading',
      button: 'select__button',
      activeState: 'is-active',
      focusState: 'is-focused',
      openState: 'is-open',
      disabledState: 'is-disabled',
      highlightedState: 'is-highlighted',
      hiddenState: 'is-hidden',
      flippedState: 'is-flipped',
      loadingState: 'is-loading',
    },
    search: false,
    itemSelectText: '',
  });
}
