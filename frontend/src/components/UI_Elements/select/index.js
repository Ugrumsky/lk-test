// import './index.scss';
import {defaultSelectElementInit} from './init';
document.addEventListener('DOMContentLoaded', (event) => {
  let elements = document.getElementsByClassName('select__field');
  for(let i = 0; i < elements.length; i++) {
    defaultSelectElementInit(elements[i]);
  }
});


