import inputMask from 'inputmask';

export function initPhoneField(container) {
  container = container || document;
  let elements = container.querySelectorAll('.input__field[type="tel"]');

  for (let i = 0; i < elements.length; i++) {
    let im = inputMask({
      mask: '+7(999)999-99-99',
      showMaskOnHover: false,
    });
    im.mask(elements[i]);
  }
  // console.log(elements);
}
