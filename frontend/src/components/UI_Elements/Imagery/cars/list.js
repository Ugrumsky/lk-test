import _mapValues from 'lodash/mapValues';

const carsObject = {
  'Golf': {
    kits: {
      'Golf R': {
        'price': '2 415 000',
        'flat': require('components/UI_Elements/Imagery/cars/Golf/golf_r.png'),
        'rotated': require('components/UI_Elements/Imagery/cars/Golf/golf_r-1.png'),
      },
      'Golf R Line': {
        'price': '1 304 000',
        'flat': require('components/UI_Elements/Imagery/cars/Golf/golf_r-line.png'),
        'rotated': require('components/UI_Elements/Imagery/cars/Golf/golf_r-line-1.png'),
      },
      'Golf Highline': {
        'price': '1 353 000',
        'flat': require('components/UI_Elements/Imagery/cars/Golf/golf_high.png'),
        'rotated': require('components/UI_Elements/Imagery/cars/Golf/golf_high-1.png'),
        'main': true,
      },
      'Golf Comfortline': {
        'price': '1 197 000',
        'flat': require('components/UI_Elements/Imagery/cars/Golf/golf_comf.png'),
        'rotated': require('components/UI_Elements/Imagery/cars/Golf/golf_comf-1.png'),
      },
      'Golf GTI': {
        'price': '1 936 000',
        'flat': require('components/UI_Elements/Imagery/cars/Golf/golf_gti.png'),
        'rotated': require('components/UI_Elements/Imagery/cars/Golf/golf_gti-1.png'),
      },
    },
  },
  'Jetta': {
    kits: {
      'Jetta Comfortline': {
        'price': '1 109 000',
        'flat': require('components/UI_Elements/Imagery/cars/Jetta/jetta_comf.png'),
        'rotated': require('components/UI_Elements/Imagery/cars/Jetta/jetta_comf-1.png'),
      },
      'Jetta Trendline': {
        'price': '989 000',
        'flat': require('components/UI_Elements/Imagery/cars/Jetta/jetta_trend.png'),
        'rotated': require('components/UI_Elements/Imagery/cars/Jetta/jetta_trend-1.png'),
      },
      'Jetta Conceptline': {
        'price': '939 000',
        'flat': require('components/UI_Elements/Imagery/cars/Jetta/jetta_concept.png'),
        'rotated': require('components/UI_Elements/Imagery/cars/Jetta/jetta_concept-1.png'),
      },
      'Jetta LIFE': {
        'price': '980 000',
        'flat': require('components/UI_Elements/Imagery/cars/Jetta/jetta_life.png'),
        'rotated': require('components/UI_Elements/Imagery/cars/Jetta/jetta_life-1.png'),
      },
      'Jetta Highline': {
        'price': '1 179 000',
        'flat': require('components/UI_Elements/Imagery/cars/Jetta/jetta_high.png'),
        'rotated': require('components/UI_Elements/Imagery/cars/Jetta/jetta_high-1.png'),
        'main': true,
      },
    },
  },
  'Passat': {
    kits: {
      'Passat Trendline': {
        'price': '1 510 000',
        'flat': require('components/UI_Elements/Imagery/cars/Passat/passat_trend.png'),
        'rotated': require('components/UI_Elements/Imagery/cars/Passat/passat_trend-1.png'),
      },
      'Passat Comfortline': {
        'price': '1 710 000',
        'flat': require('components/UI_Elements/Imagery/cars/Passat/passat_comf.png'),
        'rotated': require('components/UI_Elements/Imagery/cars/Passat/passat_comf-1.png'),
      },
      'Passat Highline': {
        'price': '1 850 000',
        'flat': require('components/UI_Elements/Imagery/cars/Passat/passat_high.png'),
        'rotated': require('components/UI_Elements/Imagery/cars/Passat/passat_high-1.png'),
        'main': true,
      },
    },
  },
  'Passat Alltrack': {
    kits: {
      'Passat Alltrack': {
        'price': '2 380 000',
        'flat': require('components/UI_Elements/Imagery/cars/Passat Alltrack/passat_alltrack.png'),
        'rotated': require('components/UI_Elements/Imagery/cars/Passat Alltrack/passat_alltrack-1.png'),
        'main': true,
      },
    },
  },
  'Passat Variant': {
    kits: {
      'Passat Variant Trendline': {
        'price': '1 790 000',
        'flat': require('components/UI_Elements/Imagery/cars/Passat Variant/passat_trend.png'),
        'rotated': require('components/UI_Elements/Imagery/cars/Passat Variant/passat_trend-1.png'),
      },
      'Passat Variant Comfortline': {
        'price': '1 940 000',
        'flat': require('components/UI_Elements/Imagery/cars/Passat Variant/passat_comf.png'),
        'rotated': require('components/UI_Elements/Imagery/cars/Passat Variant/passat_comf-1.png'),
      },
      'Passat Variant Highline': {
        'price': '2 260 000',
        'flat': require('components/UI_Elements/Imagery/cars/Passat Variant/passat_high.png'),
        'rotated': require('components/UI_Elements/Imagery/cars/Passat Variant/passat_high-1.png'),
        'main': true,
      },
    },
  },
  'Polo': {
    kits: {
      'Polo Comfortline': {
        'price': '682 900',
        'flat': require('components/UI_Elements/Imagery/cars/Polo/polo_comf.png'),
        'rotated': require('components/UI_Elements/Imagery/cars/Polo/polo_comf-1.png'),
      },
      'Polo Trendline': {
        'price': '636 900',
        'flat': require('components/UI_Elements/Imagery/cars/Polo/polo_trend.png'),
        'rotated': require('components/UI_Elements/Imagery/cars/Polo/polo_trend-1.png'),
      },
      'Polo Conceptline': {
        'price': '599 900',
        'flat': require('components/UI_Elements/Imagery/cars/Polo/polo_concept.png'),
        'rotated': require('components/UI_Elements/Imagery/cars/Polo/polo_concept-1.png'),
      },
      'Polo LIFE': {
        'price': '667 900',
        'flat': require('components/UI_Elements/Imagery/cars/Polo/polo_life.png'),
        'rotated': require('components/UI_Elements/Imagery/cars/Polo/polo_life-1.png'),
      },
      'Polo Highline': {
        'price': '779 900',
        'flat': require('components/UI_Elements/Imagery/cars/Polo/polo_high.png'),
        'rotated': require('components/UI_Elements/Imagery/cars/Polo/polo_high-1.png'),
        'main': true,
      },
      'Polo GT': {
        'price': '826 900',
        'flat': require('components/UI_Elements/Imagery/cars/Polo/polo_gt.png'),
        'rotated': require('components/UI_Elements/Imagery/cars/Polo/polo_gt-1.png'),
      },
    },
  },
  'Tiguan': {
    kits: {
      'Tiguan Trend & Fun': {
        'price': '1 129 000',
        'flat': require('components/UI_Elements/Imagery/cars/Tiguan Classic/tiguan_trend.png'),
        'rotated': require('components/UI_Elements/Imagery/cars/Tiguan Classic/tiguan_trend-1.png'),
      },
      'Tiguan CLUB': {
        'price': '1 219 000',
        'flat': require('components/UI_Elements/Imagery/cars/Tiguan Classic/tiguan_club.png'),
        'rotated': require('components/UI_Elements/Imagery/cars/Tiguan Classic/tiguan_club-1.png'),
      },
      'Tiguan Avenue': {
        'price': '1 289 000 ',
        'flat': require('components/UI_Elements/Imagery/cars/Tiguan Classic/tiguan_avenue.png'),
        'rotated': require('components/UI_Elements/Imagery/cars/Tiguan Classic/tiguan_avenue-1.png'),
        'main': true,
      },
    },
  },
  'Новый Tiguan': {
    kits: {
      'Новый Tiguan Trendline': {
        'price': '1 459 000',
        'flat': require('components/UI_Elements/Imagery/cars/Tiguan NF/tiguan_trend.png'),
        'rotated': require('components/UI_Elements/Imagery/cars/Tiguan NF/tiguan_trend-1.png'),
      },
      'Новый Tiguan Comfortline': {
        'price': '1 559 000',
        'flat': require('components/UI_Elements/Imagery/cars/Tiguan NF/tiguan_comf.png'),
        'rotated': require('components/UI_Elements/Imagery/cars/Tiguan NF/tiguan_comf-1.png'),
      },
      'Новый Tiguan Highline': {
        'price': '1 829 000',
        'flat': require('components/UI_Elements/Imagery/cars/Tiguan NF/tiguan_high.png'),
        'rotated': require('components/UI_Elements/Imagery/cars/Tiguan NF/tiguan_high-1.png'),
        'main': true,
      },
    },
  },
  'Touareg': {
    kits: {
      'Touareg': {
        'price': '2 699 000',
        'flat': require('components/UI_Elements/Imagery/cars/Touareg/touareg.png'),
        'rotated': require('components/UI_Elements/Imagery/cars/Touareg/touareg-1.png'),
      },
      'Touareg Business': {
        'price': '3 005 000 ',
        'flat': require('components/UI_Elements/Imagery/cars/Touareg/touareg_bus.png'),
        'rotated': require('components/UI_Elements/Imagery/cars/Touareg/touareg_bus-1.png'),
      },
      'Touareg R-Line': {
        'price': '3 180 000 ',
        'flat': require('components/UI_Elements/Imagery/cars/Touareg/touareg_r-line.png'),
        'rotated': require('components/UI_Elements/Imagery/cars/Touareg/touareg_r-line-1.png'),
      },
      'Touareg R-Line Executive': {
        'price': '4 082 000',
        'flat': require('components/UI_Elements/Imagery/cars/Touareg/touareg_exec.png'),
        'rotated': require('components/UI_Elements/Imagery/cars/Touareg/touareg_exec-1.png'),
        'main': true,
      },
    },
  },
};


class ModelKit {
  constructor(name, options) {
    this.name = name;
    this.flatImage = options.flat;
    this.rotatedImage = options.rotated;
    this.price = options.price;
    this.main = this.main || false;
  }
}

class Model {
  constructor(name, options) {
    let self = this;
    this.name = name;
    this.kits = _mapValues(options.kits, (kitVal, kitName) => {
      if (kitVal.main) {
        self.mainKit = kitName;
      }
      return new ModelKit(kitName, kitVal);
    });
  }

  getMainKit() {
    return this.kits[this.mainKit];
  }
}

class ModelCollection {
  constructor(carsObject) {
    this.models = _mapValues(carsObject, (carOptions, carName) => new Model(carName, carOptions));
  }
}

module.exports = new ModelCollection(carsObject);
