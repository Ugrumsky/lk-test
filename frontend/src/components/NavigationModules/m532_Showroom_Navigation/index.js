/**
 * Created by UFS on 3/13/2017.
 */
import Component, {findComponent} from '../../../globals/js/Libraries/component';
import ShowroomPage from '../../../globals/js/Libraries/showroom';
import sticky from '../../../globals/js/Libraries/sticky';
import './index.scss';

const ShowroomPageController = new ShowroomPage();

class ShowroomNav extends Component {
  constructor(el) {
    super(el);
    this.element = el;
    this.color = false;
    this.cta_container = this.element.querySelector('.js_showroom_nav_cta');
    ShowroomPageController.linkNav(this);
    sticky.add(this.element);
    this.initComponent();
    // Page.setProp('ShowroomNav', this);
  }

  applyPaddings() {

  }
}

findComponent('.m532', ShowroomNav);
