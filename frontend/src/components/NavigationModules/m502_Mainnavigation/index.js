import $ from 'jquery';

import flexibility from 'flexibility';
import Modernizr from 'modernizr';
import Media from 'globals/js/media.js';

import _debounce from 'lodash/debounce';
import _throttle from 'lodash/throttle';
import _toArray from 'lodash/toArray';
import _map from 'lodash/map';

import mainItemParse from './Nav/Item/index';

// Import m501_Logo
import 'components/NavigationModules/m501_Logo';

import NAV_ITEM from './Nav/item';

// Template import
import moreButtonTemplate from './Nav/Toggle/more.pug'; // take no params
import menuButtonTemplate from './Nav/Toggle/menu.pug'; // take no params

(() => {
    const $window = $(window);

    (() => {
        class NAV {
            constructor() {
                const self = this;

                this.logo = $('#vw_m501_logo');

                this.navs = {
                    main: $('.js_header_menu'),
                    sub: $('.js_m502_submenu'),
                };

                this.lists = {
                    main: $(this.navs.main).find('.js_m520_main_items'),
                    sub: $(this.navs.sub).find('.js_m520_sublayer_sections'),
                };

                this.items = _toArray(this.lists.main.children().map((i, el) => mainItemParse(el)));

                    this.toggle = $('<li/>');
                this.toggle_templates = {
                    more: moreButtonTemplate(),
                    menu: menuButtonTemplate(),
                };

                this.lists.main.append(this.toggle);
                this.toggle.html(this.toggle_templates.more);

                this.toggle_mobile = this.navs.main.find('.js_m502_mob_menu_switcher');


                $(() => {
                    self.init();
                });
            }

            init() {
                let self = this;

                // Polyfill nav flexbox items
                // console.log(Modernizr);
                if (!Modernizr.flexbox && !Modernizr.flexwrap && !Modernizr.flexboxlegacy) {
                    this.polyfillFlex.call(this);
                }

                $(window).resize(() => self.mobileScroll.call(self));
                self.mobileScroll.call(self);

                this.toggle.on('click', function(e) {
                    self.toggleSubMenu(e);
                });
                this.toggle_mobile.on('click', function(e) {
                    self.toggleSubMenu(e);
                });

                // console.log(this.items);

                this.items = _map(this.items, (item, i) => {
                    const itemPair = new NAV_ITEM(item, i);

                    self.lists.sub.append(itemPair.sublayer);
                    return itemPair;
                });

                $(window).on('resize', _debounce(function(e) {
                    self.flowNavItems.call(self);
                }, 200));
                self.flowNavItems();
                // console.log(this);
            }

            polyfillFlex() {
                let self = this;

                flexibility(this.navs.main.parent()[0]);
                $(window).on('resize', _debounce(function() {
                    flexibility(self.navs.main.parent()[0]);
                }, 150));

                if (process.env.NODE_ENV === 'development') {
                    console.warn('Flexbox polyfilled: m502_Mainnavigation');
                }
            }

            mobileScroll() {
                let $window = $(window);
                let self = this;

                if ($window.width() < Media.breakpoints.tablet) {
                    $window.on('scroll.hmh', _throttle(self.mobileHide(), 300));
                } else {
                    $window.off('scroll.hmh');
                    self.navs.main.removeClass('slide-up');
                }
            }

            mobileHide() {
                let lst = 0;
                let self = this;

                function scrollControl() {
                    let st = $(window).scrollTop();

                    if (st < lst || st < 150) {
                        self.navs.main.removeClass('slide-up');
                    } else {
                        self.navs.main.addClass('slide-up');
                    }
                    lst = st;
                }

                return scrollControl;
            }

            toggleSubMenu(e) {
                e.preventDefault();

                this.toggle_mobile.toggle();
                this.navs.sub.toggleClass('is-opened');
                if (Media.isMobile) {
                    $('body').toggleClass('scroll-lock');
                }
            }

            flowNavItems() {
                const self = this;
                let winWidth = $window.width();

                function resetItems() {
                    for (let i = self.items.length >= 6 ? 4 : self.items.length - 1; i > -1; i--) {
                        self.items[i].show();
                    }
                }

                function resetMoreButton() {
                    self.toggle.html(self.toggle_templates.more);
                }

                /**
                 * @this NAV
                 */
                function flowItems() {
                    const winHeight = $window.height();
                    const logoHeight = self.logo.outerHeight();
                    resetItems.call(self);
                    resetMoreButton.call(self);
                    for (let i = self.items.length >= 6 ? 4 : self.items.length - 1; i > -1; i--) {
                        if (winHeight - logoHeight < self.lists.main.outerHeight()) {
                            self.items[i].hide();
                            if (i === 0) {
                                self.toggle.html(self.toggle_templates.menu);
                            }
                        } else {
                            self.items[i].show();
                        }
                    }
                }


                if (winWidth >= Media.breakpoints.tablet) {
                    flowItems.call(self);
                }
            }
        }

        return new NAV();
    })();
})();
