/**
 * Created by Kodix on 06.03.2017.
 */

import $ from 'jquery';
import sublayerItemTemplate from './SublayerItem/index.pug';

export default class NAV_ITEM {
  constructor(parsedEl, index) {
    this.index = index;
    this.element = $(parsedEl.el);
    this.sublayer = $(sublayerItemTemplate({
      link: parsedEl.href,
      icon: parsedEl.icon,
      label: parsedEl.label,
      active: parsedEl.active,
    }));
  }

  hide() {
    this.element.addClass('js_vw_m502_nav_item_flow');
    this.sublayer.removeClass('js_vw_m502_nav_item_flow');
  }

  show() {
    this.element.removeClass('js_vw_m502_nav_item_flow');
    this.sublayer.addClass('js_vw_m502_nav_item_flow');
  }
}
