import $ from 'jquery';

$.extend($.easing, {
  easeInOutQuint: (x, t, b, c, d) => {
    if ((t/=d/2) < 1) return c/2*t*t*t*t*t + b;
    return c/2*((t-=2)*t*t*t*t + 2) + b;
  },
});

$('.js_go_to_top').on('click', function() {
  $('html, body').animate({
    scrollTop: 0,
  }, 300,
  'easeInOutQuint');
});

