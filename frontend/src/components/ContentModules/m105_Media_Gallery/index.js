/**
 * Created by ufs on 3/17/2017.
 */
import Slider from '../../../globals/js/Libraries/slider';
import Component, {findComponent} from '../../../globals/js/Libraries/component';
// Import styles
import './index.scss';

class Gallery extends Component {
  constructor(el) {
    super(el);
    this.element = $(el);
    this.inner = this.element.find('.m105__slider');
    this.dots = this.element.find('.m105__dots');
    this.controls = this.element.find('.m105__controls');
    this.slider = false;

    this.initComponent();
    this.init();
  }
  init() {
    if(!this.slider) {
      this.slider = new Slider(this.inner, {
        slidesToShow: 1,
        appendArrows: this.controls,
        appendDots: this.dots,
      });
      this.slider.mount();
    }
  }
}


findComponent('.m105', Gallery);
