/**
 * Created by ufs on 3/17/2017.
 */
import Slider from '../../../globals/js/Libraries/slider';
import Accordion from '../m108_Accordion/index';
import Component, {findComponent} from '../../../globals/js/Libraries/component';

// UI Elements
import {arrowTheme} from 'components/UI_Elements/pagination/arrows';

const arrow = arrowTheme('table');

// Import styles
import './index.scss';
let tIterator = 0;

class Table extends Component {
  constructor(el) {
    super(el);
    this.element = $(el);
    this.tableColInner = this.element.find('.m104-thead__inner');
    this.tableColControls = this.element.find('.m104-thead__controls');
    this.tableRowsInner = this.element.find('.m104-tbody__inner');
    this.accordionWrapper = this.element.find('.m104__mtable');
    this.tableIndex = tIterator;
    tIterator++;
    this.element.attr('data-tindex', this.tableIndex);
    this.slider = new Slider(this.tableColInner, {
      appendArrows: this.tableColControls,
      dots: false,
      slidesToShow: 4,
      infinite: false,
      swipe: false,
      prevArrow: arrow.left,
      nextArrow: arrow.right,
    });
    this.rowsSliders = this.tableRowsInner.toArray().map((el) => new Slider($(el), {
      dots: false,
      arrows: false,
      slidesToShow: 4,
      infinite: false,
      swipe: false,
    }));
    this.accordion = null;
    this.accordionInited = false;
    this.sliderInited = false;
    this.initComponent();
    if(this.accordionWrapper) {
      this.initAccordion();
    }
    this.initSlider();
  }
  initSlider() {
    if(!this.sliderInited) {
      this.slider.mount();
      for(let i = 0; i < this.rowsSliders.length; i++) {
        console.log(this.rowsSliders[i]);
        this.rowsSliders[i].mount();
        this.rowsSliders[i].syncTo(this.slider);
      }
      this.sliderInited = true;
    }
  }
  initAccordion() {
    if(!this.accordionInited) {
      this.accordion = new Accordion(this.accordionWrapper[0]);
      this.accordionInited = true;
    }
  }
}

findComponent('.m104', Table);
