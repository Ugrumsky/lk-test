import $ from 'jquery';
import Component, {findComponent} from '../../../globals/js/Libraries/component';
import Media from '../../../globals/js/media';
// import Accordion from '../m108_Accordion/index';
import Slider from '../../../globals/js/Libraries/slider';
// import flexibility from 'flexibility';
// import Modernizr from 'modernizr';
// import _debounce from 'lodash/debounce';

// Import styles
import './index.scss';

class CustomFeatureWrapper extends Component {
  constructor(element) {
    super(element);
    this.wrapper = $(element);
    this.inner = $(element).find('.mk154__container');
    this.controls = $(element).find('.mk154__controls');
    this.dots = $(element).find('.mk154__dots');
    this.features = this.inner.children();
    if (this.features.length === 1) {
      this.inner.addClass('is-one-element');
    } else if (this.features.length === 2) {
      this.inner.addClass('is-two-elements');
    } else if (this.features.length >= 3) {
      this.inner.addClass('is-more-than-two-elements');
    }
    // this.sliderMobileFlag = this.wrapper.hasClass('is-slider-mobile');
    // this.accordion = false;
    this.slider = false;
    this.initComponent();
    this.init();
  }

  init() {
    this.makeItSlider();
    // const self = this;
    // $(() => {
    //   $(window).on('resize', _debounce( function(e)  {
    //     const wWidth = window.innerWidth;
    //     if (wWidth < Media.breakpoints.tablet && !self.sliderMobileFlag) {
    //       self.unmountSlider();
    //       self.makeItAccordion();
    //     } else {
    //       self.unmountAccordion();
    //       self.makeItSlider();
    //     }
    //     console.log(self);
    //   }, 200));
    //
    //
    //   const wWidth = window.innerWidth;
    //   if (wWidth < Media.breakpoints.tablet && !self.sliderMobileFlag) {
    //     self.makeItAccordion();
    //   } else {
    //     self.makeItSlider();
    //   }
    //
    //   console.log(self);
    // });
  }

  makeItSlider(callback) {
    if (this.features.length > 3 || this.sliderMobileFlag) {
      if (!this.slider) {
        this.slider = new Slider(this.inner, {
          slidesToShow: 1,
          appendArrows: this.controls,
          appendDots: this.dots,
          responsive: [
            {
              breakpoint: Media.breakpoints.tablet - 1,
              settings: {
                slidesToShow: 2,
              },
            },
            {
              breakpoint: Media.breakpoints.widescreen - 1,
              settings: {
                slidesToShow: 3,
              },
            },
          ],
        });
      }
      if (callback) {
        this.slider.one('init', callback);
      }
      this.slider.mount();
    }
  }

  unmountSlider(callback) {
    if (this.slider instanceof Slider) {
      if (callback) {
        this.slider.one('destroy', callback);
      }
      this.slider.unmount();
    }
  }

  // makeItAccordion() {
  //   if (!this.accordion) {
  //     this.accordion = new Accordion(this.inner);
  //   } else {
  //     this.accordion.init();
  //   }
  // }
  //
  // unmountAccordion() {
  //   if (this.accordion instanceof Accordion) {
  //     this.accordion.destroy();
  //   }
  // }
}

findComponent('.mk154', CustomFeatureWrapper);
