import $ from 'jquery';
import _toArray from 'lodash/toArray';
import _map from 'lodash/map';
import _debounce from 'lodash/debounce';
import {addClass, removeClass} from '../../../globals/js/utility/classesMunipulation';
// import accordionWrapperTemplate from './wrapper.pug';
// import accordionItemTemplate from './item.pug';
// import accordionItemHeadTemplate from './item-head.pug';
import './index.scss';

/*
  @this {AccordionItem}
 */
function applyHeight() {
  return _debounce(this.applyHeight.bind(this), 200);
}

class AccordionItem {
  /**
   * Item for accordion
   * @param el
   * @param parent {Accordion}
   */
  constructor(el, parent) {
    const self = this;
    this.el = el;
    this.parent = parent;
    this.headContainer = this.el.firstElementChild;
    this.wrapper = this.headContainer.nextElementSibling;
    this.inner = this.wrapper.firstElementChild;
    this.opened = false;
    for(let i = 0; i < this.headContainer.children.length; i++) {
      this.headContainer.children[i].addEventListener('click', self.open.bind(self));
    }
    // console.log(this);
  }

  applyHeight() {
    const innerHeight = this.inner.offsetHeight;
    this.wrapper.style.height = parseInt(innerHeight) / 10 + 'rem';
  }

  open() {
    // const self = this;
    this.opened = true;
    this.parent.closeAll();
    addClass(this.el, 'is-opened');
    this.applyHeight();
    $(window).on('resize', applyHeight.call(this));
    $(this.wrapper).on('resizeinner', applyHeight.call(this));
    $(this.el).trigger('resizeinner');
    // console.log(this, 'opened!');
  }

  close() {
    // const self = this;
    this.opened = false;
    removeClass(this.el, 'is-opened');
    $(window).off('resize', applyHeight.call(this));
    $(this.wrapper).off('resizeinner', applyHeight.call(this));
    $(this.el).trigger('resizeinner');
    this.wrapper.style.height = '';
  }
}

export default class Accordion {
  constructor(parent, findTitleSelector = false) {
    if (!parent) {
      return false;
    }
    const self = this;
    this.container = parent;
    this.items = _map(_toArray(this.container.children || []), (node) => new AccordionItem(node, self));

    // console.log(this);
  }

  closeAll() {
    for (let i = 0; i < this.items.length; i++) {
      this.items[i].close();
    }
  }

}

// $('.m108_Accordion').each((i, el) => new Accordion(el));
