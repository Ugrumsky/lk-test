/**
 * Created by Kodix on 14.03.2017.
 */
import Component, {findComponent} from '../../../globals/js/Libraries/component';
import {hasClass} from '../../../globals/js/utility/classesMunipulation';
import ShowroomPage from '../../../globals/js/Libraries/showroom';
import Page from '../../../globals/js/Libraries/page';
import viewport from '../../../globals/js/common/viewport';
import _debounce from 'lodash/debounce';
const ShowroomPageController = new ShowroomPage();


import './index.scss';


class ShowroomStage extends Component {
    constructor(el) {
        super(el);
        this.element = el;
        this.color = false;
        this.cta_button = this.element.querySelector('.js_showroom_stage_cta_button');
        this.swapped = false;

        ShowroomPageController.linkStage(this);
        this.initComponent();


        this.swapper();
    }

    swapper() {
        const self = this;
        const nextEl = this.element.nextElementSibling;
        if (nextEl && hasClass(nextEl, 'mk504')) {
            const next = $(nextEl);
            const nextData = next.data();

            function swapOnMobile(module1, module2) {
                const windowW = viewport().width;
                if ((windowW < 768 && self.swapped === false) || (windowW >= 768 && self.swapped === true)) {
                    Page.swapModules(module1, module2);
                    self.swapped = (windowW < 768 && !self.swapped);
                }
            }

            if (nextData.hasOwnProperty('_component')) {
                $(swapOnMobile.bind(this, self, nextData._component));
                $(window).on('resize', /* @this HTMLElement */
                    _debounce(swapOnMobile.bind(this, self, nextData._component), 200));
            } else {
                next.on('_component:init', /* @this HTMLElement */ function(component) {
                    $(swapOnMobile.bind(this, self, nextData._component));
                    $(window).on('resize', /* @this HTMLElement */
                        _debounce(swapOnMobile.bind(this, self, nextData._component), 200));
                });
            }
        }
    }
}

findComponent('.m341', ShowroomStage);
