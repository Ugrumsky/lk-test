// Import styles
import './index.scss';

// Import m301_Homepage_Stage item
import './Item/index.js';

// Import modules
import $ from 'jquery';
import Component, {findComponent} from '../../../globals/js/Libraries/component';
import lazyLoadImages from '../../../globals/js/common/imageLazyLoad';
import _debounce from 'lodash/debounce';

import Slider from '../../../globals/js/Libraries/slider';


class MainpageStage extends Component {
    constructor(stage) {
        super(stage);
        let self = this;
        this.stage = $(stage);
        this.color = false;
        this.controls = this.stage.find('.vw_m301_homepage_stage_controls');
        this.inner = this.stage.find('.vw_m301_homepage_stage_inner');
        this.quickAccessBar = this.stage.find('.vw_m301_homepage_stage_qab');
        this.slides = this.inner.children();

        this.initComponent();

        /**
         * @this VWMainpageStage
         * @param el to check
         */
        function syncStageLightness(el) {
            /**
             * @this VWMainpageStage
             * @param color
             */
            function toggleColor(color) {
                this.quickAccessBar.removeClass(`is-${(color === 'light' ? 'dark' : 'light')}`);
                this.quickAccessBar.addClass(`is-${color}`);
            }

            if (el.hasClass('vw_m301_stage_item--dark')) {
                toggleColor.call(this, 'dark');
            } else {
                toggleColor.call(this, 'light');
            }
        }

        if (this.slides.length > 1) {
            this.slider = new Slider(this.inner, {
                dots: false,
                appendArrows: this.controls,
            });
            this.slider.on('init afterChange', function() {
                syncStageLightness.call(self, self.inner.find('.slick-active'));
            });
            $(() => {
                self.slider.mount();
                $(window).on('resize', _debounce(function() {
                        self.slider.reInit();
                    }, 200
                ))
                ;
                lazyLoadImages(self.inner[0], () => {
                    self.slider.reInit();
                });
            });
        } else {
            syncStageLightness.call(this, $(this.slides[0]));
        }
    }
}


findComponent('.vw_m301_homepage_stage', MainpageStage);

