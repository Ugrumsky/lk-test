(function($){
    var objStorage = {};

    var _kdxForm = {
        init: function(jWrapper, objOptions) {
            _that = this;

            if($(jWrapper).hasClass('loaded'))
                return false;

            $(jWrapper).addClass('loaded');

            var uniqID = 'kdx-form-' + Math.floor(Math.random() * 10000);
            $(jWrapper).find('form').attr('id', uniqID);

            _that.settings = $.extend({
                url:      '/ajax/getFormResult.php',
                jWrapper: $(jWrapper),
                jForm:    $(jWrapper).find("form"),
                strCode:  $(jWrapper).find("form").data('CODE') || $(jWrapper).find('form').attr('NAME'),
                type:     'POST',
                files:    {},
                strError: 'has_error',
                strMessageWrapper: '',
                strSubmitClass: 'hideme',
                objSubmitOpacity: {
                    'ACTIVE':  'N',
                    'OPACITY': .5
                },

                // Events Area. That may be called from Init.
                OnAfterFormDataCollect: function(){},

                OnBeforeFormSend: function(){},
                OnAfterFormSend:  function(){},

                OnFormSuccess: function(){},
                OnFormError:   function(){}

            }, objOptions);


            objStorage[uniqID] = _that.settings;
            var arFileInputNames = [];

            $(_that.settings.jWrapper).find("input[type='file']").on("change", function(e){
                var uniqID = $(this).parents('form').attr('id');
                objStorage[uniqID].files[$(this).attr("name")] = e.target.files;
                arFileInputNames.push($(this).attr("name"));
            });

            $(_that.settings.jForm).on("submit", function(e){
                _that.settings = objStorage[$(this).attr('id')];

                e.preventDefault();
                e.stopPropagation();

                var objCollect = _that.collect();

                _that.settings.OnAfterFormDataCollect(objCollect);
                kdxTools.GetEventHandler('dForm:OnAfterFormDataCollect', _that.settings.strCode, objCollect);
                $(_that.settings.jForm).trigger('kdx-form-after-data-collect', [_that.settings.strCode, objCollect]);

                try{
                    var formData = new FormData();
                    $.each(objCollect, function(key, object){
                        formData.append(object['name'], object['value']);
                    });

                    $.each(arFileInputNames,function(strId, strName){
                        $.each(_that.settings.files[strName], function(strFieldName, rawFileData){
                            formData.append(strName, rawFileData);
                        })
                    })
                    var boolProcessData = false;
                    var strContentType = false;

                }catch (e){
                    var formData = $(_that.settings.jForm).serialize();
                    var boolProcessData = true;
                    var strContentType = "application/x-www-form-urlencoded";
                }


                $.ajax({
                    url:  _that.settings.url,
                    type: _that.settings.type,

                    dataType: 'JSON',
                    data: formData,

                    processData: boolProcessData,
                    contentType: strContentType,

                    beforeSend: function(){
                        _that.settings.OnBeforeFormSend(_that.settings.strCode);
                        kdxTools.GetEventHandler('dForm:OnBeforeFormSend', _that.settings.strCode);
                        $(_that.settings.jForm).trigger('kdx-form-before-send', [_that.settings.strCode]);
                    },

                    success: function(json){
                        $(_that.settings.jForm).find('.' + _that.settings.strError).removeClass(_that.settings.strError);
                        $(_that.settings.jForm).find('.error').remove();

                        _that.settings.OnAfterFormSend(_that.settings.strCode, json, _that.settings.jForm);
                        kdxTools.GetEventHandler('dForm:OnAfterFormSend', _that.settings.strCode, json, _that.settings.jForm);
                        $(_that.settings.jForm).trigger('kdx-form-after-send', [_that.settings.strCode, json, _that.settings.jForm]);

                            if(json['STATUS'] === 'success'){
                            _that.settings.OnFormSuccess(_that.settings.strCode, json);
                            kdxTools.GetEventHandler('dForm:OnFormSuccess', _that.settings.strCode, json);
                            $(_that.settings.jForm).trigger('kdx-form-success', [_that.settings.strCode, json, _that.settings.jForm]);

                            var strEventName = kdxTools.Form.GetEventName(_that.settings.strCode);

                            if( !kdxTools.Form.IsAllEventsPrevented() && !kdxTools.Form.IsSingleEventPrevented(strEventName) ) {

                                kdxTools.Form.GoogleAnalyticsSend({
                                    type: 'event',
                                    name: strEventName,
                                    action: 'Submit',
                                    cat: 'KdxForms'
                                });
                                kdxTools.Form.YandexMetrika.Send(strEventName, {});

                            }

                            var target = $(_that.settings.jWrapper);

                            if(_that.settings.strMessageWrapper !== ''){
                                var tmpTarget = $(target).find(_that.settings.strMessageWrapper);

                                if(tmpTarget.length === 1)
                                    target = tmpTarget;
                            }

                            $(target).removeClass('form').html("<div>" + json['MESSAGE'] + "</div>");
                        }
                        else{

                            kdxTools.GetEventHandler('dForm:OnFormError', _that.settings.strCode, json, _that.settings.jForm);
                            _that.settings.OnFormError(_that.settings.strCode, json, _that.settings.jForm);
                            $(_that.settings.jForm).trigger('kdx-form-error', [_that.settings.strCode, json, _that.settings.jForm]);

                            if(typeof json !== 'undefined'){
                                $.each(json, function(strFieldCode, objFieldDescription){
                                    $(_that.settings.jForm).find(".field-" + strFieldCode).addClass(_that.settings.strError);
                                    $(_that.settings.jForm).find(".field-" + strFieldCode).prepend("<div class='error'><label style='color:red'>" + objFieldDescription + "</label><br></div>");

                                });
                            }
                        }
                    },

                    error: function(){

                        _that.settings.OnAfterFormSend(_that.settings.strCode, false, _that.settings.jForm);
                        kdxTools.GetEventHandler('dForm:OnAfterFormSend', _that.settings.strCode, false, _that.settings.jForm);
                        $(_that.settings.jForm).trigger('kdx-form-after-send', [_that.settings.strCode, false, _that.settings.jForm]);

                        kdxTools.GetEventHandler('dForm:OnFormError', _that.settings.strCode, false, _that.settings.jForm);
                        _that.settings.OnFormError(_that.settings.strCode, false, _that.settings.jForm);
                        $(_that.settings.jForm).trigger('kdx-form-error', [_that.settings.strCode, false, _that.settings.jForm]);
                    }
                });

            });

            return jWrapper;
        },
        collect: function(){
            return $(this.settings.jForm).serializeArray();
        }
    };

    $.fn.kdxForm = function(objOptions){
        return _kdxForm.init(this, objOptions);
    };
}(jQuery));
