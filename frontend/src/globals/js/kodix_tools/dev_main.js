
/*Удаляем картинку, если в ней есть ошибка*/
window.CheckImagesLoad = function(obj) {
    $(obj + '[src=""]').hide();
    $(obj).error(function() {
        $(this).hide()
    });
};

$(document).ready(function(){
    CheckImagesLoad('img');
    $("body").on("kdx-load", "form", function(){
        var strFormCode = $(this).attr('name');
        var strClassName = '';
        var strElementName = '';

        $(this).parent().kdxForm({
            strError: 'not_ok',
            strSubmitClass: '',
            objSubmitOpacity: {
                'ACTIVE':  'Y',
                'OPACITY': .5
            },
            OnBeforeFormSend : function(strFormCode){

                var objEvent = {
                    yandex: 'CUSTOMFORMSUBMIT',
                    google: [ strFormCode + 'Form', 'submit', window.location.pathname ]
                };

                $.each( objKdxAnalytics.preDefinedEvents.availableCars, function( strKey, objItem ){
                    if( objItem['name'] === strFormCode ){
                        objEvent = {
                            yandex: objItem['objYaLabels']['strSubmit'],
                            google: [
                                objItem['objGAParams']['objSubmit']['strCategory'],
                                objItem['objGAParams']['objSubmit']['strEvent'],
                                objItem['objGAParams']['objSubmit']['strLabel']
                            ]
                        };

                        return false;
                    }
                });
            },
            OnFormSuccess : function(strFormCode){
                var objEvent = {
                    yandex: 'CUSTOMFORMSUCCESS',
                    google: [ strFormCode + 'Form', 'success', window.location.pathname ]
                };

                $.each( objKdxAnalytics.preDefinedEvents.availableCars, function( strKey, objItem ){
                    if( objItem['name'] === strFormCode ){
                        objEvent = {
                            yandex: objItem['objYaLabels']['strSuccess'],
                            google: [
                                objItem['objGAParams']['objSuccess']['strCategory'],
                                objItem['objGAParams']['objSuccess']['strEvent'],
                                objItem['objGAParams']['objSuccess']['strLabel']
                            ]
                        };

                        return false;
                    }
                });
            }
        });
    });


});