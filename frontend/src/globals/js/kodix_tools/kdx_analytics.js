(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['jquery'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // Node. Does not work with strict CommonJS, but
    // only CommonJS-like environments that support module.exports,
    // like Node.
    module.exports = factory(require('jquery'));
  } else {
    // Browser globals (root is window)
    root.returnExports = factory(root.jQuery);
  }
}(this, function ($) {
  var objCCAnalyticsFunctions = {};
  var objKdxAnalytics = (function () {
    var objParent = {
      boolSendToClient: false,
      strSiteId: 's1',

      strKdxGoogleCounter: '',

      strYandexDoubleAnalytics: '',
      strGoogleDoubleAnalytics: '',
      strGoogleDoubleAnalyticsID: '',

      preDefinedEvents: {
        // Предопределённые события

        creditCalc: {
          name: 'CREDIT_CALC',
          objYaLabels: {
            strOpen: 'CARCREDITFORMOPEN',
            strSubmit: 'CARCREDITFORMSUBMIT',
            strSuccess: 'CARCREDITFORMSUCCESS'
          },
          objGAParams: {
            objOpen: {strCategory: 'CarCreditForm', strEvent: 'open', strLabel: window.location.pathname},
            objSubmit: {strCategory: 'CarCreditForm', strEvent: 'submit', strLabel: window.location.pathname},
            objSuccess: {strCategory: 'CarCreditForm', strEvent: 'success', strLabel: window.location.pathname}
          }
        },
        availableCars: {
          objGetModel: {
            name: 'AC_GET_MODEL',//Код формы
            objYaLabels: {
              strOpen: 'CHECKAVAILFORMOPEN',
              strSubmit: 'CHECKAVAILFORMSUBMIT',
              strSuccess: 'CHECKAVAILFORMSUCCESS'
            },//Достижение цели открытие/сабмит/успешный сабмит для Yandex
            objGAParams: {
              objOpen: {strCategory: 'CheckAvailForm', strEvent: 'open', strLabel: window.location.pathname},//Достижение цели "Открыть форму"
              objSubmit: {strCategory: 'CheckAvailForm', strEvent: 'submit', strLabel: window.location.pathname},//Достижение цели "Сабмит формы"
              objSuccess: {strCategory: 'CheckAvailForm', strEvent: 'success', strLabel: window.location.pathname}//Достижение цели "Успешный сабмит формы"
            }
          },
          objQuestions: {
            name: 'AC_QUESTIONS',
            objYaLabels: {strOpen: null, strSubmit: 'GOTQUESTIONSFORMSUBMIT', strSuccess: 'GOTQUESTIONSFORMSUCCESS'},
            objGAParams: {
              objOpen: null,
              objSubmit: {strCategory: 'GotQuestionsForm', strEvent: 'submit', strLabel: window.location.pathname},
              objSuccess: {strCategory: 'GotQuestionsForm', strEvent: 'success', strLabel: window.location.pathname}
            }
          },
          objSpecialsPrice: {
            name: 'AC_SPECIAL_PRICE',
            objYaLabels: {
              strOpen: 'WHATSMYPRICEFORMOPEN',
              strSubmit: 'WHATSMYPRICEFORMSUBMIT',
              strSuccess: 'WHATSMYPRICEFORMSUCCESS'
            },
            objGAParams: {
              objOpen: {strCategory: 'WhatsMyPriceForm', strEvent: 'open', strLabel: window.location.pathname},
              objSubmit: {strCategory: 'WhatsMyPriceForm', strEvent: 'submit', strLabel: window.location.pathname},
              objSuccess: {strCategory: 'WhatsMyPriceForm', strEvent: 'success', strLabel: window.location.pathname}
            }
          },
          objOrder: {
            name: 'AC_ORDER',
            objYaLabels: {
              strOpen: 'BOOKAUTOFORMOPEN',
              strSubmit: 'BOOKAUTOFORMSUBMIT',
              strSuccess: 'BOOKAUTOFORMSUCCESS'
            },
            objGAParams: {
              objOpen: {strCategory: 'BookAutoForm', strEvent: 'open', strLabel: window.location.pathname},
              objSubmit: {strCategory: 'BookAutoForm', strEvent: 'submit', strLabel: window.location.pathname},
              objSuccess: {strCategory: 'BookAutoForm', strEvent: 'success', strLabel: window.location.pathname}
            }
          },
          objGetPhoto: {
            name: 'AC_GET_PHOTO',
            objYaLabels: {
              strOpen: 'MAILPHOTOFORMOPEN',
              strSubmit: 'MAILPHOTOFORMSUBMIT',
              strSuccess: 'MAILPHOTOFORMSUCCESS'
            },
            objGAParams: {
              objOpen: {strCategory: 'MailPhotoForm', strEvent: 'open', strLabel: window.location.pathname},
              objSubmit: {strCategory: 'MailPhotoForm', strEvent: 'submit', strLabel: window.location.pathname},
              objSuccess: {strCategory: 'MailPhotoForm', strEvent: 'success', strLabel: window.location.pathname}
            }
          },
          objTestDrive: {
            name: 'AC_TESTDRIVE',
            objYaLabels: {
              strOpen: 'TESTDRIVEFORMOPEN',
              strSubmit: 'TESTDRIVEFORMSUBMIT',
              strSuccess: 'TESTDRIVEFORMSUCCESS'
            },
            objGAParams: {
              objOpen: {strCategory: 'TestDriveForm', strEvent: 'open', strLabel: window.location.pathname},
              objSubmit: {strCategory: 'TestDriveForm', strEvent: 'submit', strLabel: window.location.pathname},
              objSuccess: {strCategory: 'TestDriveForm', strEvent: 'success', strLabel: window.location.pathname}
            }
          },
          objGetDetail: {
            name: 'AC_DETAIL',
            objYaLabels: {
              strOpen: 'CLARIFYDETAILSFORMOPEN',
              strSubmit: 'CLARIFYDETAILSFORMSUBMIT',
              strSuccess: 'CLARIFYDETAILSFORMSUCCESS'
            },
            objGAParams: {
              objOpen: {strCategory: 'ClarifyDetailsForm', strEvent: 'open', strLabel: window.location.pathname},
              objSubmit: {strCategory: 'ClarifyDetailsForm', strEvent: 'submit', strLabel: window.location.pathname},
              objSuccess: {strCategory: 'ClarifyDetailsForm', strEvent: 'success', strLabel: window.location.pathname}
            }
          },
          objGetSpecialsDetail: {
            name: 'AC_SPECIALS_DETAIL',
            objYaLabels: {
              strOpen: 'SPECIALSDETAILFORMOPEN',
              strSubmit: 'SPECIALSDETAILFORMSUBMIT',
              strSuccess: 'SPECIALSDETAILFORMSUCCESS'
            },
            objGAParams: {
              objOpen: {strCategory: 'SpecialsDetailForm', strEvent: 'open', strLabel: window.location.pathname},
              objSubmit: {strCategory: 'SpecialsDetailForm', strEvent: 'submit', strLabel: window.location.pathname},
              objSuccess: {strCategory: 'SpecialsDetailForm', strEvent: 'success', strLabel: window.location.pathname}
            }
          },
          objOrderForm: {
            name: 'CAR_ORDER',
            objYaLabels: {
              strOpen: 'NEWCARORDERFORMOPEN',
              strSubmit: 'NEWCARORDERFORMSUBMIT',
              strSuccess: 'NEWCARORDERFORMSUCCESS'
            },
            objGAParams: {
              objOpen: {strCategory: 'NewCarOrderForm', strEvent: 'open', strLabel: window.location.pathname},
              objSubmit: {strCategory: 'NewCarOrderForm', strEvent: 'submit', strLabel: window.location.pathname},
              objSuccess: {strCategory: 'NewCarOrderForm', strEvent: 'success', strLabel: window.location.pathname}
            }
          },
          objTDForm: {
            name: 'TESTDRIVE',
            objYaLabels: {
              strOpen: 'TESTDRIVEFORMOPEN',
              strSubmit: 'TESTDRIVEFORMSUBMIT',
              strSuccess: 'TESTDRIVEFORMSUCCESS'
            },
            objGAParams: {
              objOpen: {strCategory: 'TestDriveForm', strEvent: 'open', strLabel: window.location.pathname},
              objSubmit: {strCategory: 'TestDriveForm', strEvent: 'submit', strLabel: window.location.pathname},
              objSuccess: {strCategory: 'TestDriveForm', strEvent: 'success', strLabel: window.location.pathname}
            }
          },
          objMaintenanceForm: {
            name: 'SIGNING_FOR_SERVICE',
            objYaLabels: {
              strOpen: 'MAINTENANCEFORMOPEN',
              strSubmit: 'MAINTENANCEFORMSUBMIT',
              strSuccess: 'MAINTENANCEFORMSUCCESS'
            },
            objGAParams: {
              objOpen: {strCategory: 'MaintenanceForm', strEvent: 'open', strLabel: window.location.pathname},
              objSubmit: {strCategory: 'MaintenanceForm', strEvent: 'submit', strLabel: window.location.pathname},
              objSuccess: {strCategory: 'MaintenanceForm', strEvent: 'success', strLabel: window.location.pathname}
            }
          },
          objCallbackForm: {
            name: 'CALLBACK',
            objYaLabels: {
              strOpen: 'CALLBACKFORMOPEN',
              strSubmit: 'CALLBACKFORMSUBMIT',
              strSuccess: 'CALLBACKFORMSUCCESS'
            },
            objGAParams: {
              objOpen: {strCategory: 'CallbackForm', strEvent: 'open', strLabel: window.location.pathname},
              objSubmit: {strCategory: 'CallbackForm', strEvent: 'submit', strLabel: window.location.pathname},
              objSuccess: {strCategory: 'CallbackForm', strEvent: 'success', strLabel: window.location.pathname}
            }
          },
          objFeedbackForm: {
            name: 'FEEDBACK',
            objYaLabels: {
              strOpen: 'CALLBACKFORMOPEN',
              strSubmit: 'CALLBACKFORMSUBMIT',
              strSuccess: 'CALLBACKFORMSUCCESS'
            },
            objGAParams: {
              objOpen: {strCategory: 'CallbackForm', strEvent: 'open', strLabel: window.location.pathname},
              objSubmit: {strCategory: 'CallbackForm', strEvent: 'submit', strLabel: window.location.pathname},
              objSuccess: {strCategory: 'CallbackForm', strEvent: 'success', strLabel: window.location.pathname}
            }
          }
        },
        defaultClick: {
          objTabSpecialsClick: {
            strSelector: 'a[href="#special_block"]',
            objYaLabels: 'SHOWSPECIALSBUTTONCLICK',
            objGAParams: {strCategory: 'ShowSpecialsButton', strEvent: 'click', strLabel: window.location.pathname}
          },
          objPDFDownload: {
            strSelector: 'a[href*=".pdf"]',
            objYaLabels: 'PDFDOWNLOAD',
            objGAParams: {strCategory: 'PDF', strEvent: 'Download', strLabel: window.location.pathname}
          }
        }
      },

      init: function (objParams) {
        this.boolSendToClient = ( objParams['KM_KDX_DOUBLE_CENTRAL_ANALYTICS'] === true || objParams['KM_KDX_DOUBLE_CENTRAL_ANALYTICS'] === 'true' );
        this.strSiteId = objParams['SITE_ID'];

        this.strYandexDoubleAnalytics = objParams['YANDEX_DOUBLE_ANALYTICS'];
        this.strGoogleDoubleAnalytics = objParams['GOOGLE_DOUBLE_ANALYTICS'];
        this.strGoogleDoubleAnalyticsID = objParams['GOOGLE_DOUBLE_ANALYTICS_ID'];


        this.compatibility();
        this.events();
      },

      events: function () {
        // Привязка любых событий

        $.each(this.preDefinedEvents.defaultClick, function (strKey, objItem) {
          addHandler(objItem);
        });

        objCCAnalyticsFunctions['CCFormSubmit'] = function () {
          YaSend(objParent.preDefinedEvents.creditCalc.objYaLabels.strSubmit);
          GaSend(objParent.preDefinedEvents.creditCalc.objGAParams.objSubmit.strCategory,
            objParent.preDefinedEvents.creditCalc.objGAParams.objSubmit.strEvent,
            objParent.preDefinedEvents.creditCalc.objGAParams.objSubmit.strLabel);
        };

        objCCAnalyticsFunctions['CCFormSuccess'] = function () {
          YaSend(objParent.preDefinedEvents.creditCalc.objYaLabels.strSuccess);
          GaSend(objParent.preDefinedEvents.creditCalc.objGAParams.objSuccess.strCategory,
            objParent.preDefinedEvents.creditCalc.objGAParams.objSuccess.strEvent,
            objParent.preDefinedEvents.creditCalc.objGAParams.objSuccess.strLabel);
        };

        $(document).on('click', 'a[data-kdx-button-xml]', function (e) {
          var _that = $(this).data('kdx-button-xml');
          var strYaLabel = '';

          switch (_that) {
            case 'AVN' :
              strYaLabel = 'GOTOAVNBUTTON';
              break;
            case 'CC' :
              strYaLabel = 'GOTOCCBUTTON';
              break;
            default :
              strYaLabel = null;
          }

          if (strYaLabel !== null)
            YaSend(strYaLabel);

          GaSend('Goto' + _that + 'Button', 'click', window.location.pathname);
        });
      },

      compatibility: function () {
        // Совместимость со старыми глобальными переменными.

        var objSiteToName = {
          's1': 'pkw',
          'nf': 'nf',
          'mn': 'mn',
          'mp': 'mp'
        };

        if (this.boolSendToClient === true)
          window['double_analytics'] = true;

        if (this.strYandexDoubleAnalytics !== '')
          window['ya_double_analytics_id_' + objSiteToName[this.getSiteId()]] = this.strYandexDoubleAnalytics;

        if (this.strGoogleDoubleAnalytics !== '')
          window['ga_double_analytics_command_' + objSiteToName[this.getSiteId()]] = this.strGoogleDoubleAnalytics;
      },

      getSiteId: function () {
        return this.strSiteId;
      },

      isSendToClient: function () {
        return ( this.boolSendToClient === true );
      },

      objYandex: {
        's1': 'yaCounter23062024',
        'nf': 'yaCounter36248485',
        'mn': 'yaCounter36248540',
        'mp': 'yaCounter36248355'
      },

      haveYandexCounter: function () {
        return ( typeof this.objYandex[this.getSiteId()] !== 'undefined' && typeof window[this.objYandex[this.getSiteId()]] !== 'undefined' );
      },

      getYandexCounter: function () {
        return window[this.objYandex[this.getSiteId()]];
      },

      client: {
        // По факту - является тем местом, в котором ОБЯЗАТЕЛЬНО должна сохраняться обратная совместимость
        // Можно даже заменить часть методов дилерскими - и сделать это будет достаточно просто
        getSiteId: function () {
          return objParent.getSiteId();
        },

        isSendToClient: function () {
          return objParent.isSendToClient();
        },

        haveYandexAnalytics: function () {
          return ( objParent.strYandexDoubleAnalytics !== '' && typeof window[objParent.strYandexDoubleAnalytics] !== 'undefined' );
        },
        getYandexAnalyticsCounter: function () {
          return window[objParent.strYandexDoubleAnalytics];
        },
        getYandexAnalyticsCounterID: function () {
          return objParent.strYandexDoubleAnalytics;
        },
        haveGa: function () {
          return ( typeof window['ga'] !== 'undefined' );
        },
        haveGoogleAnalytics: function () {
          return ( this.haveGa() && objParent.strGoogleDoubleAnalytics !== '' );
        },
        getGoogleAnalytics: function () {
          return ( objParent.strGoogleDoubleAnalytics );
        },
        getGoogleAnalyticsID: function () {
          return ( objParent.strGoogleDoubleAnalyticsID );
        }
      }
    };

    return objParent;
  })();

  function YaSend(strLabel) {
    if (objKdxAnalytics.client.isSendToClient() && objKdxAnalytics.client.haveYandexAnalytics())
      objKdxAnalytics.client.getYandexAnalyticsCounter().reachGoal(strLabel);

    /*if( objKdxAnalytics.haveYandexCounter() )
     objKdxAnalytics.getYandexCounter().reachGoal( strLabel );*/
  }

  function GaSend(strCategory, strEvent, strLabel) {
    if (objKdxAnalytics.client.isSendToClient() && objKdxAnalytics.client.haveGoogleAnalytics())
      ga(objKdxAnalytics.client.getGoogleAnalytics(), 'event', strCategory, strEvent, strLabel);

    console.log('KDX.send', 'event', strCategory, strEvent, strLabel);

    /*ga('KDX.send', 'event', strCategory, strEvent, strLabel);*/
  }

  function addHandler(objHandler) {
    if (typeof objHandler === 'object') {
      $(document).on('click', objHandler.strSelector, function () {
        /* if( objHandler.objYaLabels !== null )
         YaSend( objHandler.objYaLabels );

         GaSend( objHandler.objGAParams.strCategory, objHandler.objGAParams.strEvent, objHandler.objGAParams.strLabel );*/
      });
    }
  }

  return {
    objCCAnalyticsFunctions: objCCAnalyticsFunctions,
    objKdxAnalytics: objKdxAnalytics
  }
}));
