/**
 * Created by ufs on 3/24/2017.
 */

class Freezer {
    constructor() {
      this.active = false;
    }
    lock() {
      if(!this.active) {
        document.body.style.overflow = 'hidden';
        document.documentElement.style.overflow = 'hidden';
        this.active = true;
      }
    }
    unlock() {
      if(this.active) {
        document.body.style.overflow = '';
        document.documentElement.style.overflow = '';
        this.active = false;
      }
    }
}

export default new Freezer();
