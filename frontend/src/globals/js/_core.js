// Global js functionality
import $ from 'jquery';
import 'components/UI_Elements/input/index.js';
import 'components/UI_Elements/select/index.js';

import lazyLoadImages from './common/imageLazyLoad';
$(() => {
  lazyLoadImages();
});

// Fixes
import './common/touchfix';
import './common/browserDetect';


// Common page functionality
import Page from './Libraries/page';
import PowerLayer from './Libraries/powerlayer';

window['Page'] = Page;
window['PowerLayer'] = PowerLayer;
