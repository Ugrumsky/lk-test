/**
 * Created by UFS on 3/13/2017.
 */

import _throttle from 'lodash/throttle';
import _delay from 'lodash/delay';

let instance = null;

export default class ShowroomPage {
    constructor() {
        if(!instance) {
            instance = this;
        }
        this.nav = false;
        this.stage = false;

        return instance;
    }
    linkNav(nav) {
        this.nav = nav;
        this.checkInit();
    }
    linkStage(stage) {
        this.stage = stage;
        this.checkInit();
    }
    checkInit() {
        if(!!this.nav && !!this.stage) {
            this.init();
        }
    }
    init() {
        const self = this;
        const ctaContainer = $(self.nav.cta_container);
        $(this.stage.cta_button).clone().appendTo(this.nav.cta_container);
        function ctaStageMove() {
            const $stage = $(self.stage.element);
            const $nav = $(self.nav.element);
            const stageHeight = $stage.outerHeight();
            const stageOffset = $stage.position().top;
            const navHeight = $nav.outerHeight();
            const scrollTop = $(window).scrollTop();

            if(stageHeight + stageOffset - navHeight < scrollTop) {
                ctaContainer.addClass('is-opened');
            } else {
                ctaContainer.removeClass('is-opened');
                _delay(() => {
                    self.nav.applyPaddings();
                }, 200);
            }
        }

        $(window).on('scroll', _throttle(ctaStageMove, 200));
    }
};
