/**
 * Created by ufs on 3/22/2017.
 */
import url from 'url';
import $ from 'jquery';
require('imports-loader!history.js/history.js');
require('imports-loader!history.js/history.adapter.ender.js');
// import
// import modernizr from 'modernizr';
import freezer from '../utility/freezeBody';
import '../../style/vwd5/_powerlayer.scss';


class PowerLayer {
    constructor(href, controller) {
        this.url = href;
        this.urlParsed = url.parse(this.url);
        console.log(this.urlParsed);
        this.controller = controller;
        this.wrapper = null;
        this.dom = null;
        this.opened = false;
    }

    open(actionSrc) {
        console.log('opened');

        const self = this;
        actionSrc.setAttribute('disabled', true);
        if (this.dom === null) {
            $.ajax({
                url: self.url,
                dataType: 'html',
                success: function(data) {
                    self.dom = data;
                    actionSrc.removeAttribute('disabled');
                    self.generateDOM(actionSrc);
                },
                error() {
                    actionSrc.removeAttribute('disabled');
                },
            });
        } else {
            actionSrc.removeAttribute('disabled');
            this.generateDOM(actionSrc);
        }
    }

    close() {
        if (this.opened === true) {
            this.wrapper.remove();
            freezer.unlock();
            this.wrapper = null;
            this.opened = false;
        }
    }

    generateDOM(actionSrc) {
        // const self = this;
        freezer.lock();
        this.wrapper = $('<div/>', {
            class: 'm400',
        }).appendTo($('body'))
            .html(this.dom);
        this.wrapper.find('.m400-close')
            .on('click', /* @this HTMLElement */ function(e) {
                e.preventDefault();
                window.history.back();
            });
        window.history.pushState(null, null, url.parse(actionSrc.href).path);
        this.opened = true;
        this.controller.opened = this.urlParsed.pathname;
    }
}

class PowerLayerController {
    constructor() {
        this.powerLayers = {};
        this.opened = null;
        const self = this;
        const powerLayerLinks = document.getElementsByClassName('js_powerlayer_link');
        for (let i = 0; i < powerLayerLinks.length; i++) {
            const powerLayerLink = url.parse(powerLayerLinks[i].href || '');
            // console.log(powerLayerLink);
            if (!this.powerLayers.hasOwnProperty(powerLayerLink.pathname)) {
                this.powerLayers[powerLayerLink.pathname] = new PowerLayer(powerLayerLink.href, this);
            }
            powerLayerLinks[i].addEventListener('click', /* @this HTMLElement */function(e) {
                e.preventDefault();
                self.powerLayers[url.parse(this.href).pathname].open(this);
            });
        }

        window.addEventListener('popstate', function(event) {
            const state = event.state;
            console.log(event);
            if (state === null) {
                self.powerLayers[self.opened].close();
                self.opened = null;
            }
        });
        //
        // history.listen((location, action) => {
        //   // location is an object like window.location
        //   console.log(action, location.pathname, location.state);
        //   if (location.pathname === '/' && self.opened !== null) {
        //     self.powerLayers[self.opened].close();
        //     self.opened = null;
        //   }
        //   if (this.powerLayers.hasOwnProperty(location.pathname)) {
        //     self.opened = location.pathname;
        //   }
        // });
    }
}

export default new PowerLayerController();
