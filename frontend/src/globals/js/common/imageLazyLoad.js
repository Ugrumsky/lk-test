// Polifills
import obf from 'object-fit-images';
import picturefill from 'picturefill';
import {addClass} from '../utility/classesMunipulation';
import _toArray from 'lodash/toArray';
import _isFuction from 'lodash/isFunction';

function _setSources(element, srcsetDataAttribute, srcDataAttribute) {
    let tagName = element.tagName;
    let elementSrc = element.getAttribute('data-' + srcDataAttribute);

    function _setSourcesForPicture(element, srcsetDataAttribute) {
        let parent = element.parentElement;
        if (parent.tagName !== 'PICTURE') {
            return;
        }
        for (let i = 0; i < parent.children.length; i++) {
            let pictureChild = parent.children[i];
            if (pictureChild.tagName === 'SOURCE') {
                let sourceSrcset = pictureChild.getAttribute('data-' + srcsetDataAttribute);
                if (sourceSrcset) {
                    pictureChild.setAttribute('srcset', sourceSrcset);
                }
            }
        }
    }

    if (tagName === 'IMG') {
        _setSourcesForPicture(element, srcsetDataAttribute);
        let imgSrcset = element.getAttribute('data-' + srcsetDataAttribute);
        if (imgSrcset) element.setAttribute('srcset', imgSrcset);
        if (elementSrc) element.setAttribute('src', elementSrc);
        return;
    }
    if (tagName === 'IFRAME') {
        if (elementSrc) element.setAttribute('src', elementSrc);
        return;
    }
    element.style.backgroundImage = 'url(' + elementSrc + ')';
}

class ImageLazyLoader {
    constructor(container = document, callback = false) {
        this.images = _toArray(container.querySelectorAll('img[data-src]'));
        this.queue = this.images.length;
        this.callback = callback;

        this.init();
    }

    init() {
        const self = this;
        for (let i = 0; i < self.images.length; i++) {
            let img = self.images[i];

            _setSources(img, 'srcset', 'src');


            img.onload = function() {
                picturefill({
                    elements: [this],
                });
                obf(this);
                addClass(this.parentNode.parentNode, 'is-loaded');
                self.queue--;
                this.onload = null;
                if (_isFuction(self.callback) && self.queue <= 0) {
                    self.callback();
                }
            };
        }
    }
}

export default function lazyLoadImages(container = document, callback) {
    new ImageLazyLoader(container, callback);
};


