import $ from 'jquery';
import _debounce from 'lodash/debounce';
const $document = $(document);
const $window = $(window);

function isTouch() {
  let touchsupport = ('ontouchstart' in window) || (navigator.maxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0);
  if (!touchsupport) { // browser doesn't support touch
    $('html').addClass('no-touch');
  } else {
    $('html').removeClass('no-touch');
  }
}

$document.ready(isTouch);
$window.resize(_debounce(isTouch, 200));
