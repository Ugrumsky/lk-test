// Main gulp components
import  fs from 'fs'
import  path from 'path'
import yargs from 'yargs';
import  gulp from 'gulp'
import  glob from 'glob'
import  _ from 'lodash'
import  merge2 from 'merge2';

const argv = yargs.argv;
const $ = require('gulp-load-plugins')();

// Utility components
// var livereload = require('gulp-livereload'),
//     runSequence = require('run-sequence');

// Compile modernizr script
// gulp.task('modernizr', function () {
//   return gulp.src('./frontend/build/assets/css/**/*.css')
//     .pipe($.modernizr(
//       {
//         cache: false,
//         "options": [
//           "html5shiv",
//           "setClasses"
//         ],
//         "crawl": true,
//         "excludeTests": ['hidden']
//       }
//     ))
//     // .pipe($.print())
//     .pipe($.uglify())
//     .pipe(gulp.dest('./frontend/build/assets/js/'));
// });

gulp.task('iconfont', function () {
    var stream = merge2();

    var pathToFonts = path.join(__dirname, 'src', 'globals', 'icons');
    if (fs.existsSync(pathToFonts)) {
        var fontDirs = getDirectories(pathToFonts);
        for (var k = 0; k < fontDirs.length; k++) {
            stream.add(
                gulp.src([path.relative(__dirname, fontDirs[k].path).replace('\\', '/') + '/*.svg'])
                    .pipe($.iconfontCss({
                        fontName: fontDirs[k].name,
                        path: 'src/devdep/gulp/_icons.scss',
                        targetPath: './stylesheet.scss',
                        cssClass: fontDirs[k].name,
                        fontPath: './',
                    }))
                    .pipe($.iconfont({
                        fontName: fontDirs[k].name,
                        formats: ['ttf', 'woff', 'woff2'],
                        descent: fontDirs[k].settings.descent !== 'default' ? fontDirs[k].settings.descent : 0,
                        fixedWidth: fontDirs[k].settings.monospaced
                    }))
                    .pipe($.if('stylesheet.scss', $.splitFiles()))
                    .pipe(gulp.dest(path.join(__dirname, 'src', 'globals', 'fonts', 'icons', fontDirs[k].name.replace('icon-', ''))))
            )
        }

        stream.on('finish', function () {
            var icons = {};
            for (var i = 0; i < fontDirs.length; i++) {
                var name = fontDirs[i].name.replace('icon-', '');
                _.map(glob.sync('*.svg', {
                    cwd: fontDirs[i].path
                }), function (icon) {
                    icon = icon.replace('.svg', '');
                    if (!icons.hasOwnProperty(name))
                        icons[name] = {};
                    icons[name][icon] = 'icon-' + name + '-' + icon;
                })
            }

            fs.writeFileSync(path.join(__dirname, 'src', 'globals', 'fonts', 'icons', 'icons.json'), JSON.stringify(icons, null, 2));
        })

    }

    return stream;

    function getDirectories(srcpath) {
        return fs.readdirSync(srcpath).filter(function (file) {
            return fs.statSync(path.join(srcpath, file)).isDirectory() && fs.readdirSync(path.join(srcpath, file)).length > 0;
        }).map(function (file) {
            return {
                name: 'icon-' + file,
                path: path.join(srcpath, file),
                settings: fs.existsSync(path.join(srcpath, file, '_settings.json')) ? require(path.join(srcpath, file, '_settings.json')) : {
                        "descent": "default",
                        "monospaced": false
                    }
            }
        });
    }
});
