## Сборщик проектов Kodix

Проект использует в качестве ядра *browser-sync* + *webpack* c hmr.

Из технологий используются *pug*, *scss*, *babel ES6*

###Установка
```bash
npm i
```
или
```bash
yarn
```

### Запуск
Дилер:   
`start` для запуска dev сервера, `build` для сборки для html.kodix, `deploy` для продакшена 
   
Импортер:   
`start-im`, `build-im`, `deploy-im` аналогично скриптам для дилера 



####Конфиг сборщика
Настройки для сборщика находятся в файле kdx.config.json, структура:

kdx.config.json [default]:
```json
{
  "default": {
    "paths": {
      "sourceRoot": "src/", // корневая папка src файлов. все папки в ней становятся алиасами для прощего доступа
      "entries": "src/entries_dealer/", // папка с входными файлами js
      "pages": "src/pages_dealer/", // папка с шаблонами страниц см. ниже как формировать ее структуру
      "output": "build/", // в какую папку собирать проект 
      "publicPath": "/", // корневая папка на сервере в которой будут лежать css, js итд
      "assets": {
        "js": "js/", 
        "css": "css/",
        "fonts": "fonts/",
        "images": "images/",
        "templates": ""
      },
      "resolve": { // объект для создания быстрых алиасов без залезания в сборщик
        "tools": "./src/globals/style/_tools.scss",
        "inputmask.dependencyLib": "./node_modules/jquery.inputmask/dist/inputmask/inputmask.dependencyLib",
        "inputmask" : "./node_modules/jquery.inputmask/dist/inputmask/inputmask"
      },
      "hashes": false // включение hash у всех файлов. очень осторожно!
    },
    "kdxTools": false, // включение помощника kodix: возможность добавления сетки и быстрая навигация по страницам шаблона
    "devServer": {
      "active": false, // запуск development сервера
      "port": 8080 // порт с которым запускается сервер
    },
    "minify": false, // минификация всех файлов
    "provide": {}, // передание в js дополнительных параметров см. https://webpack.js.org/plugins/provide-plugin/
    "externals": {}, // указываем сборщику что модуль присутствует в глобальном контексте сайта и 
    //его не надо добавлять в сборку, а брать оттуда. Например: "jquery": "jQuery" <название модуля>: <глобальная переменная>
    "libs": [] // прекомпиляция dll.js в котором будут те модули которые не надо перезагружать каждый раз. Оптимизация
  },
  "development": { // При запуске с определенным NODE_ENV его значение будет выбрано при загрузке этого файла
    "kdxTools": true,
    "minify": false,
    "libs": [
      "jquery"
    ],
    "devServer": {
      "active": true
    }
  },
  "test": {
    "paths": {
      "publicPath": "/_auto/d5/"
    },
    "libs": [
      "jquery"
    ],
    "kdxTools": true,
    "minify": false
  },
  "production": {
    "paths": {
      "publicPath": "/local/templates/vwd5/",
      "assets": {
        "templates": "templates/"
      }
    },
    "minify": true,
    "externals": {
      "jquery": "jQuery"
    }
  }
}
```

Разбор основной структуры на примере дилера

```
-entries_dealer

 |__index внутри каждой есть index.js который является точкой входа для сборщика   <———————— 
 |                                                                                          |
 |__editoral                                                                                |
                                                                                            |
-pages_dealer                                                                               |
 |__index                                                                                   |
 |  |__index.pug - название файла устанавливает скрипт обработчик страницы index.pug -> index.js
 |
 |__showroom
    |__specs
        |__editorial.pug - путь от корня выставляет publicPath скомпилированного файла 
        pages_dealer/showroom/specs -> /showroom/specs.html           
```
