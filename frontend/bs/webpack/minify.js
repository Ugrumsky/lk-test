/**
 * Created by UFS on 3/11/2017.
 */

// Webpack
import webpack from 'webpack';

export default function (CONFIG) {
    return {
        plugins: [
            new webpack.optimize.UglifyJsPlugin({
                comments: false,
                compress: {
                    warnings: false,
                    drop_console: true
                }
            }),
            new webpack.optimize.OccurrenceOrderPlugin()
        ]
    }
}
