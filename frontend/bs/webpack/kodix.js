/**
 * Created by UFS on 3/11/2017.
 */
import path from 'path';
import _ from 'lodash';
import templatesPath from './utils/pages';
import webpack from 'webpack';

export default function (CONFIG) {
    return {
        entry: {
            commons: [
                path.resolve(__dirname, '../injects/kdx-tools/kdx-helper.js')
            ]
        },
        plugins: [
            new webpack.DefinePlugin({
                KDX_TOOLS: CONFIG.kdxTools,
                PAGES: JSON.stringify(templatesPath(CONFIG))
            }),
        ]
    };
}