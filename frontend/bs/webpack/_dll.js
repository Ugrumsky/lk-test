/**
 * Created by UFS on 3/11/2017.
 */
// Common packages
import fs from 'fs';
import path from 'path';
import yargs from 'yargs';

// Utility packages
import _ from 'lodash';
import util from 'util';
import glob from 'glob';

// NodeEnvironment
import NODE_ENV from '../environment.js';

// Builder config
import bsCONFIG from '../config.json';

// Webpack instance
import webpack from 'webpack';

// Webpack plugins
import AssetsPlugin from 'assets-webpack-plugin';
import ProgressBarPlugin from 'progress-bar-webpack-plugin';


export default function (CONFIG) {
    const outputPath = path.resolve(__dirname, '../../', CONFIG.paths.output);


    return {
        entry: {
            dll: CONFIG.libs,
        },
        output: {
            path: outputPath,
            publicPath: CONFIG.paths.publicPath,
            filename: CONFIG.paths.assets.js + '[name]' + (CONFIG.paths.hashes ? '.[chunkhash]' : '') + '.js',
            library: 'dll'
        },
        plugins: [
            new webpack.DllPlugin({
                name: 'dll',
                path: path.resolve(CONFIG.paths.output, '[name]-manifest.json')
            }),
            new AssetsPlugin({
                filename: 'dll-assets.json',
                path: path.resolve(CONFIG.paths.output),
                prettyPrint: true
            }),
            new ProgressBarPlugin(),
        ],
        devtool: !CONFIG.minify && CONFIG.devServer.active ? bsCONFIG.webpackSourceMapType : false
    };
}