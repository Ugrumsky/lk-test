/**
 * Created by UFS on 3/11/2017.
 */
// Common packages
import fs from 'fs';
import path from 'path';

// Utility packages
import _ from 'lodash';
import glob from 'glob';

const COMMON_RESOLVE = {
    modernizr$: (function () {
        const  modernizrLocalRC = path.resolve(__dirname, "../../.modernizrrc");
        return fs.existsSync(modernizrLocalRC) ? modernizrLocalRC : path.resolve(__dirname, "../default/.modernizrrc");
    }())
};



export default function (CONFIG) {
    const SRC_FOLDERS_RESOLVE = _.mapValues(_.keyBy(glob.sync(CONFIG.paths.sourceRoot + '*/', {
        cwd: process.cwd()
    }), function (filePath) {
        const pathSplitted = filePath.split('/');
        return pathSplitted[pathSplitted.length - 2];
    }), function (filePath) {
        return path.join(process.cwd(), filePath);
    });
    var aliases = _.assign(COMMON_RESOLVE, SRC_FOLDERS_RESOLVE);
    // Add common resolve
    _.each(CONFIG.paths.resolve, function (resolveItem, name) {
        if (Object.keys(aliases).indexOf(name) != -1) {
            console.log('\nWarning: Custom alias ' + name.toUpperCase() + ' overrides default.\n');
        }
        aliases[name] = path.resolve(process.cwd(), resolveItem);
    });
    return {
        resolve: {
            extensions: ['.webpack.js', '.web.js', '.js', '.jsx', '.scss', '.ts'],
            alias: aliases
        }
    };
}
