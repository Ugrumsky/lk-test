/**
 * Created by UFS on 3/11/2017.
 */
import webpack from 'webpack';

export default function (CONFIG) {

    return {
        entry: {
            commons: [
                'webpack/hot/dev-server'
            ]
        },
        resolve: {
            unsafeCache: true
        },
        plugins: [
            new webpack.HotModuleReplacementPlugin(),
            new webpack.NamedModulesPlugin(),
            new webpack.NoEmitOnErrorsPlugin()
        ]
    }
}
