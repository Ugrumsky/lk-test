/**
 * Created by UFS on 3/11/2017.
 */

import webpack from 'webpack';
import path from 'path';

export default function (CONFIG) {
    return {
        plugins: [
            new webpack.DllReferencePlugin({
                context: process.cwd(),
                manifest: require(path.resolve(__dirname, '../../', CONFIG.paths.output, 'dll-manifest.json'))
            })
        ]
    }
}