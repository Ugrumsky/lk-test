/**
 * Created by UFS on 3/12/2017.
 */
import path from 'path';
import fs from 'fs';
import _ from 'lodash';
import templatePaths from './utils/pages';
import HtmlWebpackIncludeAssetsPlugin from 'html-webpack-include-assets-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';

export default function (CONFIG) {
    let plugins = _.map(templatePaths(CONFIG), (v) => new HtmlWebpackPlugin({
        filename: v.filename,
        template: v.template,
        chunks: v.chunks
    }));
    if (CONFIG.hasOwnProperty('libs') && CONFIG.libs.length > 0) {
        plugins.push(new HtmlWebpackIncludeAssetsPlugin({
            assets:_.map(JSON.parse(
                fs.readFileSync(path.resolve(__dirname, '../../', CONFIG.paths.output, 'dll-assets.json'), 'utf8')
            ).dll, function (v, key) {
                return v.replace(CONFIG.paths.publicPath, '')
            }),
            append: false
        }));
    }

    return {
        plugins: plugins
    }
}