/*
 * Get environment variable
 */

if(!process.env.NODE_ENV || new RegExp("^dev.*$", "i").test(process.env.NODE_ENV))
    process.env.NODE_ENV = 'development';


export default process.env.NODE_ENV.trim();
