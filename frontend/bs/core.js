/**
 * Created by UFS on 3/5/2017.
 * Fedor Ushakov <ushakovfserg@gmail.com>
 */

/*
    IMPORTS
 */
// Common packages
import fs from 'fs';
import path from 'path';
import yargs from 'yargs';

// Utility packages
import _ from 'lodash';
import log, {logsClean} from './log';

// Webpack Core launch script
import webpack from './webpack';
import zipDir from 'zip-dir';

// NodeEnvironment
import NODE_ENV from './environment.js';

/*
    CONSTANTS
 */
// Additional arguments
const argv = yargs.argv;


/*
    VARIABLES
 */
// Path to local config
let CONFIG = JSON.parse(fs.readFileSync(path.resolve(__dirname, '../', (argv.config ? `kdx.config.${argv.config}.json` : 'kdx.config.json')), 'utf8'));

/*
    UTILITY PART
 */
if (argv.debug) {
    logsClean();
}

/*
    MAIN PART
 */

/*
    TODO: remove it when DeprecationWarning: loaderUtils.parseQuery() will be removed
 */
process.noDeprecation = true;
/*
    *********************************************
 */

// Get config according to Node Environment variable
CONFIG = _.defaultsDeep(CONFIG[NODE_ENV], CONFIG.default);


if(argv.zip) {
    const dateNow = new Date();
    const tempDir = path.resolve(__dirname, '../.temp/');

    function prepDate(n) {
        return (n < 10) ? '0' + n : n;
    }


    if (!fs.existsSync(tempDir)){
        fs.mkdirSync(tempDir);
    }

    console.log(path.resolve(tempDir, `build_${[prepDate(dateNow.getDate()), prepDate(dateNow.getMonth() + 1), dateNow.getFullYear()].join('_')}_${dateNow.getTime()}.zip`));



    zipDir(path.resolve(__dirname, '../', CONFIG.paths.output), {
        saveTo: path.resolve(tempDir, `build_${[prepDate(dateNow.getDate()), prepDate(dateNow.getMonth() + 1), dateNow.getFullYear()].join('_')}_${dateNow.getTime()}.zip`)
    }, function(err, buffer) {
        if (err) {
            console.log(err);
            return;
        }

        return buffer;
    });
} else {
    console.log('\nLaunching builder with settings for ' + NODE_ENV.toUpperCase() + ' environment.\n');


    if (argv.debug) {
        log('kdx_config', CONFIG);
    }

// Launch webpack script
    webpack(CONFIG);
}


